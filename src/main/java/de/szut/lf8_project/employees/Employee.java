package de.szut.lf8_project.employees;

import lombok.Data;

@Data
public class Employee {
    private long id;
    private String surname;
    private String firstname;
    private Qualification[] skillSet;
}
