package de.szut.lf8_project.employees;


import de.szut.lf8_project.exceptionHandling.ResourceNotFoundException;
import org.keycloak.authorization.client.util.Http;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.server.ServerErrorException;

@Service
public class EmployeeService {
    private final RestTemplate restTemplate;
    private String url = "https://employee.szut.dev/employees/";
public EmployeeService(){
    this.restTemplate = new RestTemplate();
}

public ResponseEntity<Employee> getEmployeeById(Long id, String authHeader){
   try {
       HttpHeaders headers = new HttpHeaders();
       String tokenSplit = authHeader.split(" ")[1];
       headers.setBearerAuth(tokenSplit);
       HttpEntity<String> entity = new HttpEntity<>("body", headers);
       return this.restTemplate.exchange(url + id, HttpMethod.GET, entity, Employee.class);
   }
   catch (HttpClientErrorException e){
       switch (e.getRawStatusCode()){
           case 404:
               throw new ResourceNotFoundException(String.format("Employee %s does not exist", id));
            default:
                throw new ServerErrorException("Error trying to reach Employee Service", e);
       }
   }
}

public ResponseEntity<Employee> getEmployeeQualifications(Long employeeId, String authHeader){
    try{
        HttpHeaders headers = new HttpHeaders();
        String tokenSplit = authHeader.split(" ")[1];
        headers.setBearerAuth(tokenSplit);
        HttpEntity<String> entity = new HttpEntity<>("body", headers);
        return this.restTemplate.exchange(url + employeeId + "/qualifications", HttpMethod.GET, entity, Employee.class);
    }
    catch (HttpClientErrorException e){
        switch (e.getRawStatusCode()){
            case 404:
                throw new ResourceNotFoundException(String.format("Employee %s does not exist", employeeId));
            default:
                throw new ServerErrorException("Error trying to reach Employee Service", e);
        }
    }
}

}
