package de.szut.lf8_project.config;

import de.szut.lf8_project.ProjektVerwaltung.EmployeeRepository;
import de.szut.lf8_project.ProjektVerwaltung.ProjectEntity;
import de.szut.lf8_project.ProjektVerwaltung.ProjectRepository;
import de.szut.lf8_project.ProjektVerwaltung.ProjectToEmployeeAssocitationEntity;
import de.szut.lf8_project.hello.HelloEntity;
import de.szut.lf8_project.hello.HelloRepository;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class SampleDataCreator implements ApplicationRunner {

    private HelloRepository repository;
    private ProjectRepository projectRepository;
    private EmployeeRepository employeeRepository;

    public SampleDataCreator(HelloRepository repository, ProjectRepository projectRepository, EmployeeRepository employeeRepository) {
        this.repository = repository;
        this.projectRepository = projectRepository;
        this.employeeRepository = employeeRepository;
    }

    public void run(ApplicationArguments args) {
        repository.save(new HelloEntity("Hallo Welt!"));
        repository.save(new HelloEntity("Schöner Tag heute"));
        repository.save(new HelloEntity("FooBar"));

        ProjectEntity projectEntity = new ProjectEntity("Test",
                2, "Person", "TestKommentar",
                new Date(), new Date(), new Date());
        ProjectEntity projectEntity2 = new ProjectEntity("Projekt2",
                3, "Person", "TestKommentar2",
                new Date(), new Date(), new Date());

        projectRepository.save(projectEntity);
        projectRepository.save(projectEntity2);

        employeeRepository.save(new ProjectToEmployeeAssocitationEntity(projectEntity, 1, "Angular"));
        employeeRepository.save(new ProjectToEmployeeAssocitationEntity(projectEntity, 2, "Spring Boot"));

        employeeRepository.save(new ProjectToEmployeeAssocitationEntity(projectEntity2, 1, "Java"));
    }
}
