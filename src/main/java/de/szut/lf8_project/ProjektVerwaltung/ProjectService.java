package de.szut.lf8_project.ProjektVerwaltung;

import de.szut.lf8_project.ProjektVerwaltung.dto.ProjectGetDto;
import de.szut.lf8_project.ProjektVerwaltung.mapper.ProjectMapper;
import de.szut.lf8_project.employees.EmployeeService;
import de.szut.lf8_project.exceptionHandling.ResourceNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ServerErrorException;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;


@Service
public class ProjectService {
    private final ProjectRepository repository;
    private final EmployeeRepository employeeRepository;
    private final EmployeeService employeeService;
    private ProjectMapper projectMapper;

    public ProjectService(ProjectRepository repository, EmployeeRepository employeeRepository) {
        this.repository = repository;
        this.employeeRepository = employeeRepository;
        this.employeeService = new EmployeeService();
        this.projectMapper = new ProjectMapper();
    }

    public List<ProjectEntity> readAll() {
        return this.repository.findAll();
    }

    //public void save(ProjectEntity project) { this.repository.save(project); }

    public ProjectEntity create(ProjectEntity project, String authHeader){
        ValidateEmployees(project, authHeader);
        ValidateCustomers(project.getCustomerId(),authHeader);
      var createdProject =  this.repository.save(project);
    this.employeeRepository.saveAll(project.getProjectToEmployeeAssocitationEntity());
    return createdProject;
    }

    public void ValidateEmployees(ProjectEntity project, String authHeader) {
        //check if employees exist
        var employeesToValidate = project.getProjectToEmployeeAssocitationEntity();

           for (ProjectToEmployeeAssocitationEntity employeeToTest :
                   employeesToValidate) {
               ValidateExistingEmployee(employeeToTest, authHeader);
               ValidateBusyEmployees(employeeToTest);
               ValidateExistingQualifications(employeeToTest, authHeader);
           }
    }
    private void ValidateCustomers(long customerId, String authHeader){
        //TODO Dummy Method to Validate Customer Data
    }

    public void ValidateExistingEmployee(ProjectToEmployeeAssocitationEntity employeeToTest, String authHeader) {
         employeeService.getEmployeeById(employeeToTest.getEmployeeId(), authHeader);
    }
    public void ValidateExistingQualifications(ProjectToEmployeeAssocitationEntity employeeToTest, String authHeader) {
        var qualifications = employeeService.getEmployeeQualifications(employeeToTest.getEmployeeId(), authHeader);
       var test = Arrays.stream(qualifications.getBody().getSkillSet()).map(o -> o.designation).collect(Collectors.toList());
        if (!test.contains(employeeToTest.getRole())) {
            throw new ResourceNotFoundException(String.format("Employee %s is not able to fullfill role: '%s'", employeeToTest.getEmployeeId(), employeeToTest.getRole()));
        }
    }

    public void ValidateBusyEmployees(ProjectToEmployeeAssocitationEntity employeeToTest) {
        List<ProjectGetDto> employeeProjects =  this.readAll()
                .stream()
                .map(e -> projectMapper.mapToGetDto(e))
                .filter(f -> f.getEmployees().stream().map(o -> o.employeeId).collect(Collectors.toList())
                        .contains(employeeToTest.getEmployeeId()))
                .collect(Collectors.toList());
        for (ProjectGetDto p : employeeProjects) {

            if (p.getEmployees().stream().map(o -> o.employeeId).collect(Collectors.toList()).contains(employeeToTest.getEmployeeId())) {
                // Ein Mitarbeiter kann nur zu einem anderen Projekt hinzugefügt werden,
                // wenn er nicht noch in einem anderen offen Projekt (Endatum null) ist.
                if (p.getEndDate() == null) {
                    throw new ResourceNotFoundException(String.format("The Employee %s is already assigned to a running Project",employeeToTest.getId()));
                }
            }
        }
    }

    public void save(ProjectEntity project) { this.repository.save(project); }

    public void save(ProjectToEmployeeAssocitationEntity employee, String authHeader) {
        ValidateExistingEmployee(employee, authHeader);
        ValidateExistingQualifications(employee, authHeader);
        this.employeeRepository.save(employee); }

    public void delete(ProjectToEmployeeAssocitationEntity employee) { this.employeeRepository.delete(employee);}

    public void delete(ProjectEntity project) { this.repository.delete(project); }

    public ProjectEntity findById(long id) {


        Optional<ProjectEntity> project = this.repository.findById(id);
        if (project.isEmpty()) {
            return null;
        }
        return project.get();
    }
}
