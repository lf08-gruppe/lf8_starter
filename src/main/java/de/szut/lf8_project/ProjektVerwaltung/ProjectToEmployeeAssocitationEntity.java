package de.szut.lf8_project.ProjektVerwaltung;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@NoArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "ProjectToEmployeeAssocitation")
public class ProjectToEmployeeAssocitationEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @ManyToOne
    private ProjectEntity projectEntity;
    private long employeeId;
    private String role;

    public ProjectToEmployeeAssocitationEntity(ProjectEntity projectEntity, long employeeId, String role) {
        this.projectEntity = projectEntity;
        this.employeeId = employeeId;
        this.role = role;
    }
}
