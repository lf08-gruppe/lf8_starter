package de.szut.lf8_project.ProjektVerwaltung;

import de.szut.lf8_project.hello.HelloEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProjectRepository extends JpaRepository<ProjectEntity, Long> {

}