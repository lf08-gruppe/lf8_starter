package de.szut.lf8_project.ProjektVerwaltung.mapper;

import de.szut.lf8_project.ProjektVerwaltung.dto.EmployeeProject;
import de.szut.lf8_project.ProjektVerwaltung.dto.ProjectByEmployeeDto;
import de.szut.lf8_project.ProjektVerwaltung.dto.ProjectGetDto;

import java.util.ArrayList;
import java.util.List;

public class ProjectToProjectByEmployeeMapper {

    public ProjectByEmployeeDto Map(long id, List<ProjectGetDto> projects) {
        var employeeProject = new ProjectByEmployeeDto();
        employeeProject.setEmployeeId(id);
        employeeProject.setProjects(new ArrayList());
        for (ProjectGetDto p :
                projects) {
            var e = new EmployeeProject();
            e.setProjectId(p.getId());
            e.setName(p.getDescription());
            e.setStartDate(p.getStartDate());
            e.setEndDate(p.getEndDate());
            var em = p.getEmployees().stream().filter(o -> o.employeeId == id).findFirst();
            if (em != null) {
                e.setQualification(em.get().role);
            }
            employeeProject.getProjects().add(e);
        }
        return employeeProject;
    }
}
