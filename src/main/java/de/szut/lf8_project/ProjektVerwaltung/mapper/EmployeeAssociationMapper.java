package de.szut.lf8_project.ProjektVerwaltung.mapper;

import de.szut.lf8_project.ProjektVerwaltung.ProjectEntity;
import de.szut.lf8_project.ProjektVerwaltung.ProjectToEmployeeAssocitationEntity;
import de.szut.lf8_project.ProjektVerwaltung.dto.ProjectCreateDto;
import de.szut.lf8_project.ProjektVerwaltung.dto.ProjectEmployeeDto;
import de.szut.lf8_project.ProjektVerwaltung.dto.ProjectToEmployeeAssocitationGetDto;

import java.util.ArrayList;
import java.util.List;

public class EmployeeAssociationMapper {
    public List<ProjectToEmployeeAssocitationEntity> mapProjectEmployeeToAssociation(ProjectCreateDto dto, ProjectEntity projectEntity) {
         List<ProjectToEmployeeAssocitationEntity> employees = new ArrayList<>();
        for (ProjectEmployeeDto projectEmployee:
              dto.getProjectEmployees()) {

            employees.add(new ProjectToEmployeeAssocitationEntity(projectEntity, projectEmployee.employeeId, projectEmployee.role));
        }
        
        
        return employees;
    }
}
