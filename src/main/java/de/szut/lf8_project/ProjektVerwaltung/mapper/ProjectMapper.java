package de.szut.lf8_project.ProjektVerwaltung.mapper;

import de.szut.lf8_project.ProjektVerwaltung.ProjectEntity;
import de.szut.lf8_project.ProjektVerwaltung.ProjectToEmployeeAssocitationEntity;
import de.szut.lf8_project.ProjektVerwaltung.dto.ProjectCreateDto;
import de.szut.lf8_project.ProjektVerwaltung.dto.ProjectEmployeeDto;
import de.szut.lf8_project.ProjektVerwaltung.dto.ProjectGetDto;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ProjectMapper {

    public ProjectGetDto mapToGetDto(ProjectEntity entity) {
        List<ProjectToEmployeeAssocitationEntity> employeeList=
            entity.getProjectToEmployeeAssocitationEntity();
        List<ProjectEmployeeDto> employees = new ArrayList<>();
        if(employeeList != null){
            for (ProjectToEmployeeAssocitationEntity employee:
                 employeeList) {
                employees.add(new ProjectEmployeeDto(employee.getEmployeeId(),employee.getRole()));
            }
        }
        return new ProjectGetDto(entity.getId(), entity.getDescription(), employees,
                entity.getCustomerId(), entity.getResponsiblePerson(), entity.getComment(),
                entity.getStartDate(), entity.getPlannedEndDate(), entity.getEndDate());
    }

        public ProjectEntity mapCreateDtoToEntity(ProjectCreateDto dto) {
            var employeeAssociationMapper = new EmployeeAssociationMapper();
            var entity = new ProjectEntity();
            entity.setComment(dto.getComment());
            entity.setDescription(dto.getDescription());
            entity.setEndDate(dto.getEndDate());
            entity.setCustomerId(dto.getCustomerId());
            entity.setPlannedEndDate(dto.getPlannedEndDate());
            entity.setResponsiblePerson(dto.getResponsiblePerson());
            entity.setStartDate(dto.getStartDate());
            entity.setProjectToEmployeeAssocitationEntity(employeeAssociationMapper.mapProjectEmployeeToAssociation(dto, entity));
            return entity;
        }

    }
