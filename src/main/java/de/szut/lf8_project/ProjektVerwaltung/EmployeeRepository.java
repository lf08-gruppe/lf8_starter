package de.szut.lf8_project.ProjektVerwaltung;

import org.springframework.data.jpa.repository.JpaRepository;

public interface EmployeeRepository extends
        JpaRepository<ProjectToEmployeeAssocitationEntity, Long> {

}