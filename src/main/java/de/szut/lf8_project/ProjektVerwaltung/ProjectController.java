package de.szut.lf8_project.ProjektVerwaltung;

import de.szut.lf8_project.ProjektVerwaltung.dto.ProjectByEmployeeDto;
import de.szut.lf8_project.ProjektVerwaltung.dto.ProjectEmployeeDto;
import de.szut.lf8_project.ProjektVerwaltung.mapper.ProjectMapper;
import de.szut.lf8_project.ProjektVerwaltung.dto.ProjectCreateDto;
import de.szut.lf8_project.ProjektVerwaltung.dto.ProjectGetDto;
import de.szut.lf8_project.ProjektVerwaltung.mapper.ProjectToProjectByEmployeeMapper;
import de.szut.lf8_project.exceptionHandling.ResourceNotFoundException;
import de.szut.lf8_project.hello.dto.HelloGetDto;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import javassist.tools.web.BadHttpRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "project")
public class ProjectController {
    private final ProjectService service;
    private final ProjectMapper projectMapper;

    public ProjectController(ProjectService service, ProjectMapper mappingService) {
        this.service = service;
        this.projectMapper = mappingService;
    }

    @Operation(summary = "delivers a list of projects")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "list of projects",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ProjectGetDto.class))}),
            @ApiResponse(responseCode = "401", description = "not authorized",
                    content = @Content)})
    @GetMapping
    public List<ProjectGetDto> findAll() {
        return this.service
                .readAll()
                .stream()
                .map(e -> this.projectMapper.mapToGetDto(e))
                .collect(Collectors.toList());
    }

    @Operation(summary = "Finds a project by Id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Returns Project with the given Id",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ProjectGetDto.class))}),
            @ApiResponse(responseCode = "404", description = "Project Id does not exist",
                    content = @Content),
            @ApiResponse(responseCode = "401", description = "not authorized",
                    content = @Content)})
    @GetMapping("/findById")
    public ProjectGetDto findProjectById(@RequestParam long id) {
        ProjectEntity projectEntity = this.service.findById(id);
        if(projectEntity == null) {
            throw new ResourceNotFoundException("Projekt mit der Id '" + id + "' konnte nicht gefunden werden");
        }
        return projectMapper.mapToGetDto(projectEntity);
    }

    @Operation(summary = "Finds all projects of an employee by Id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Returns all projects of an employee",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ProjectByEmployeeDto.class))}),
            @ApiResponse(responseCode = "404", description = "Employee hasn't worked in any project",
                    content = @Content),
            @ApiResponse(responseCode = "401", description = "not authorized",
                    content = @Content)})
    @GetMapping("/findEmployeeProjects")
    public ProjectByEmployeeDto findEmployeeProject(@RequestParam long id) {
        ProjectToProjectByEmployeeMapper mapper = new ProjectToProjectByEmployeeMapper();
        List<ProjectGetDto> employeeProjects =  this.service
                .readAll()
                .stream()
                .map(e -> this.projectMapper.mapToGetDto(e))
                .filter(f -> f.getEmployees().stream().map(o -> o.employeeId).collect(Collectors.toList())
                        .contains(id))
                .collect(Collectors.toList());


        if(employeeProjects.size() == 0) {
            throw new ResourceNotFoundException("Mitarbeiter mit der Id '" + id + "' ist in keinem Projekt beteiligt.");
        }

        return mapper.Map(id, employeeProjects);
    }

    @Operation(summary = "Liste mit Mitarbeitern zu einem Projekt hinzufügen, wenn sie in dem " +
            "Projektzeitraum nicht in einem anderen Projekt sind. Als Ergebnis Meldung ausgegeben.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Returns Message",
                    content = {@Content(mediaType = "text/html")}),
            @ApiResponse(responseCode = "400", description = "Fehler beim Hinzufügen",
                    content = @Content),
            @ApiResponse(responseCode = "401", description = "not authorized",
                    content = @Content)})
    @PutMapping("/addEmployeesToProject")
    public ResponseEntity<String> addEmployeesToProject(@RequestParam long projectId, @RequestBody @Valid ProjectEmployeeDto[] employees, @RequestHeader("Authorization") String authHeader) {
          ResponseEntity<String> result;
          StringBuilder resultMessage = new StringBuilder();
          resultMessage.append(" <p> ");

          ProjectEntity project = this.service.findById(projectId);

          if (project == null) {
              throw new ResourceNotFoundException("Das Projekt mit Id  '" + projectId + "' existiert nicht");
          }
          ProjectGetDto projectDto = this.projectMapper.mapToGetDto(project);

          List<ProjectGetDto> otherProjects = this.service.readAll().
                  stream().
                  filter(f -> f.getId() != projectId).
                  map(e -> this.projectMapper.mapToGetDto(e)).
                  collect(Collectors.toList());

          ArrayList<ProjectEmployeeDto> addEmployees = new ArrayList<>();
          boolean anyError = false;

          for (ProjectEmployeeDto employee : employees) {
              boolean hasError = false;

              if (projectDto.getEmployees().stream().map(o -> o.employeeId).collect(Collectors.toList()).contains(employee.employeeId)) {
                  String m = "Der Mitarbeiter mit Id '" + employee.employeeId +
                          "' ist bereits in diesem Projekt";
                  resultMessage.append(m + " <br> ");
                  hasError = true;
                  anyError = true;
                  continue;
              }

              for (ProjectGetDto p : otherProjects) {

                  if (projectDto.getEmployees().stream().map(o -> o.employeeId).collect(Collectors.toList()).contains(employee.employeeId)) {
                      // Ein Mitarbeiter kann nur zu einem anderen Projekt hinzugefügt werden,
                      // wenn er nicht noch in einem anderen offen Projekt (Endatum null) ist.
                      if (p.getEndDate() == null) {
                          String m = "Der Mitarbeiter mit Id '" + employee.employeeId +
                                  "' ist im noch nicht abgeschlossen Projekt '" + p.getId() + "'";
                          resultMessage.append(m + " <br> ");

                          hasError = true;
                          anyError = true;
                          break;
                      }
                  }

              }


              if (!hasError) {
                  addEmployees.add(employee);
              }
          }

          // Nur wenn es bei keinem Mitarbeiter einen Fehler gab (schon in anderem Projekt oder existiert nicht),
          // werden alle Mitarbeiter gespeichert.
          if (!anyError) {
              for (ProjectEmployeeDto employeeDto : addEmployees) {
                  ProjectToEmployeeAssocitationEntity entity =
                          new ProjectToEmployeeAssocitationEntity(project, employeeDto.employeeId, employeeDto.role);

                  this.service.save(entity, authHeader);

                  project.addEmployee(employeeDto); // Projekt Entity updaten

                  String m = "Der Mitarbeiter mit Id '" + employeeDto.employeeId +
                          "' wurde dem Projekt hinzugefügt";
                  resultMessage.append(m + " <br> ");
              }

              resultMessage.append(" </p> ");
              result = ResponseEntity.ok().body(resultMessage.toString());
          } else {
              resultMessage.append(" </p> ");
              result = ResponseEntity.badRequest().body(resultMessage.toString());
          }
          return result;



    }

    @Operation(summary = "Liste mit Mitarbeitern aus einem Projekt entfernen. Als Ergebnis Meldung ausgegeben.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Returns Message",
                    content = {@Content(mediaType = "text/html")}),
            @ApiResponse(responseCode = "400", description = "Fehler beim Entfernen",
                    content = @Content),
            @ApiResponse(responseCode = "401", description = "not authorized",
                    content = @Content)})
    @DeleteMapping("/removeEmployeesFromProject")
    public ResponseEntity<String> removeEmployeesFromProject(@RequestParam long projectId, @RequestParam Long[] employeeIds) {
        ResponseEntity<String> result;
        StringBuilder resultMessage = new StringBuilder();
        resultMessage.append("<p> ");

        ProjectEntity project = this.service.findById(projectId);
        if(project == null) {
            resultMessage.append("Das Projekt mit Id  '" + projectId + "' existiert nicht" + " </p> ");
            result = ResponseEntity.badRequest().body(resultMessage.toString());
            return result;
        }
        ProjectGetDto projectDto = this.projectMapper.mapToGetDto(project);
        List<ProjectToEmployeeAssocitationEntity> employeeAssocitationList =
                project.getProjectToEmployeeAssocitationEntity();

        ArrayList<ProjectToEmployeeAssocitationEntity> removeList = new ArrayList<>();
        boolean hasAnyError = false;

        var existingEmployeeIds = projectDto.getEmployees().stream().map(n -> n.employeeId).collect(Collectors.toList());
        for(Long employeeId : employeeIds) {
            if(existingEmployeeIds.contains(employeeId)) {
                for(ProjectToEmployeeAssocitationEntity e : employeeAssocitationList) {
                    if(e.getEmployeeId() == employeeId) {
                        removeList.add(e);
                        break;
                    }
                }
            } else {
                hasAnyError = true;
                String m = "Der Mitarbeiter mit Id '" + employeeId +
                        "' existiert nicht im Projekt";
                resultMessage.append(m + " <br> ");
            }
        }

        if(!hasAnyError) {
            for(ProjectToEmployeeAssocitationEntity remove : removeList) {
                if(remove != null) {
                    this.service.delete(remove);
                    String m = "Der Mitarbeiter mit Id '" + remove.getEmployeeId() +
                            "' wurde aus dem dem Projekt entfernt";
                    resultMessage.append(m + " <br> ");
                } else {
                    result = ResponseEntity.badRequest().body("Fehler beim Mitarbeiter Entfernen");
                    return result;
                }
            }

            resultMessage.append(" </p> ");
            result = ResponseEntity.ok().body(resultMessage.toString());
        } else {
            resultMessage.append(" </p> ");
            result = ResponseEntity.badRequest().body(resultMessage.toString());
        }

        return result;
    }

    @Operation(summary = "Updates values of the Project. All Parameters Except Id are optional. Dateformat yyyy-MM-dd")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Returns updated Project",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ProjectGetDto.class))}),
            @ApiResponse(responseCode = "404", description = "Project Id does not exist",
                    content = @Content),
            @ApiResponse(responseCode = "401", description = "not authorized",
                    content = @Content)})
    @PutMapping("/update")
    public ProjectGetDto updateProjectById(@RequestParam long id,
                                           @RequestParam Optional<String> description,
                                           @RequestParam Optional<Long> customerId,
                                           @RequestParam Optional<String> responsiblePerson,
                                           @RequestParam Optional<String> comment,
                                           @RequestParam Optional<String> startDate,
                                           @RequestParam Optional<String> plannedEndDate,
                                           @RequestParam Optional<String> endDate) {

        ProjectEntity projectEntity = this.service.findById(id);
        if(projectEntity == null) {
            throw new ResourceNotFoundException("Projekt mit der Id '" + id + "' konnte nicht gefunden werden");
        }

        if(description.isPresent()) { projectEntity.setDescription(description.get()); }
        if(customerId.isPresent()) { projectEntity.setCustomerId(customerId.get()); }
        if(responsiblePerson.isPresent()) { projectEntity.setResponsiblePerson(responsiblePerson.get()); }
        if(comment.isPresent()) { projectEntity.setComment(comment.get()); }
        if(startDate.isPresent()) {
            Date start = convertDate(startDate.get());
            if(start != null) { projectEntity.setStartDate(start); }
        }
        if(plannedEndDate.isPresent()) {
            Date planned = convertDate(plannedEndDate.get());
            if(planned != null) { projectEntity.setPlannedEndDate(planned); }
        }
        if(endDate.isPresent()) {
            Date end = convertDate(endDate.get());
            if(end != null) { projectEntity.setEndDate(end); }
        }

        this.service.save(projectEntity);
        return projectMapper.mapToGetDto(projectEntity);
    }

    private Date convertDate(String date) {
        DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd");
        Date d = null;

        try {
            d = dateFormatter.parse(date);
        }catch(Exception e) {
            throw new ResourceNotFoundException("Invalid Date");
        }

        return d;
    }

    @Operation(summary = "Deletes a project by Id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Returns updated Project",
                    content = {@Content(mediaType = "application/text",
                            schema = @Schema(implementation = ProjectGetDto.class))}),
            @ApiResponse(responseCode = "404", description = "Project Id does not exist",
                    content = @Content),
            @ApiResponse(responseCode = "401", description = "not authorized",
                    content = @Content)})
    @DeleteMapping("/delete")
    public ResponseEntity<String> deleteProject(@RequestParam long projectId) {
        ProjectEntity projectEntity = this.service.findById(projectId);

        if(projectEntity == null) {
            throw new ResourceNotFoundException("Projekt mit der Id '" + projectId + "' konnte nicht gefunden werden");
        }

        ResponseEntity<String> result;

        List<ProjectToEmployeeAssocitationEntity> employeeAssocitationList =
                projectEntity.getProjectToEmployeeAssocitationEntity();

        for(ProjectToEmployeeAssocitationEntity projectToEmployeeEntity: employeeAssocitationList) {
            this.service.delete(projectToEmployeeEntity);
        }

        this.service.delete(projectEntity);

        result = ResponseEntity.ok().body("Das Projekt mit Id: '" + projectId + "'" + " wurde gelöscht.");
        return result;
    }

    @Operation(summary = "creates a new project")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "created project",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = HelloGetDto.class))}),
            @ApiResponse(responseCode = "400", description = "invalid JSON posted",
                    content = @Content),
            @ApiResponse(responseCode = "401", description = "not authorized",
                    content = @Content)})
    @PostMapping
    public ProjectGetDto create(@RequestBody @Valid ProjectCreateDto project, @RequestHeader("Authorization") String authHeader) {

        ProjectEntity projectEntity = this.projectMapper.mapCreateDtoToEntity(project);
        projectEntity = this.service.create(projectEntity, authHeader);
        return this.projectMapper.mapToGetDto(projectEntity);
    }

}
