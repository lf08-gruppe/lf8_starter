package de.szut.lf8_project.ProjektVerwaltung;

import de.szut.lf8_project.ProjektVerwaltung.dto.ProjectEmployeeDto;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Target;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@NoArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "project")

public class ProjectEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String description;
    private long customerId;
    private String responsiblePerson;
    private String comment;
    private Date startDate;
    private Date plannedEndDate;
    private Date endDate;
    @OneToMany(mappedBy = "projectEntity")
    private List<ProjectToEmployeeAssocitationEntity> projectToEmployeeAssocitationEntity;

    public ProjectEntity(String description, long customerId,
                         String responsiblePerson, String comment, Date startDate,
                         Date plannedEndDate, Date endDate) {
        this.description = description;
        this.customerId = customerId;
        this.responsiblePerson = responsiblePerson;
        this.comment = comment;
        this.startDate = startDate;
        this.plannedEndDate = plannedEndDate;
        this.endDate = endDate;
    }

    public void addEmployee(ProjectEmployeeDto employee) {
        this.projectToEmployeeAssocitationEntity.add(
                new ProjectToEmployeeAssocitationEntity(this, employee.employeeId, employee.role));
    }
}
