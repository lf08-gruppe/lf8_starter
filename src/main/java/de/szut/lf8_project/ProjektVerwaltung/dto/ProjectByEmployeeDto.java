package de.szut.lf8_project.ProjektVerwaltung.dto;

import de.szut.lf8_project.employees.Qualification;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.Date;

@Getter
@Setter
public class ProjectByEmployeeDto {
private long employeeId;
private ArrayList<EmployeeProject> projects;

}

