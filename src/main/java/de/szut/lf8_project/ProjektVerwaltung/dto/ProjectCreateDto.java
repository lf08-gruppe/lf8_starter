package de.szut.lf8_project.ProjektVerwaltung.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;
import java.util.List;

@Getter
@Setter
public class ProjectCreateDto {
    private String description;
    private List<ProjectEmployeeDto> projectEmployees;
    private long customerId;
    private String responsiblePerson;
    private String comment;
    private Date startDate;
    private Date plannedEndDate;
    private Date endDate;
}
