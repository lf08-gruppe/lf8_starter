package de.szut.lf8_project.ProjektVerwaltung.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProjectToEmployeeAssocitationGetDto {
    private long id;
    private long projectId;
    private long employeeId;
    private String role;

    public ProjectToEmployeeAssocitationGetDto(int id, int projectId, int employeeId, String role ) {
        this.id = id;
        this.projectId = projectId;
        this.employeeId = employeeId;
        this.role = role;
    }
}
