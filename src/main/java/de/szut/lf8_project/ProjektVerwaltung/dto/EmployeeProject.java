package de.szut.lf8_project.ProjektVerwaltung.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Setter
@Getter
public class EmployeeProject {
    private long projectId;
    private String name;
    private Date startDate;
    private Date endDate;
    private String qualification;
}
