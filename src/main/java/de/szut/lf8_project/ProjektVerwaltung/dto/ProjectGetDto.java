package de.szut.lf8_project.ProjektVerwaltung.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;
import java.util.List;

@Getter
@Setter
public class ProjectGetDto {

    private long id;
    private String description;
    private List<ProjectEmployeeDto> employees;
    private long customerId;
    private String responsiblePerson;
    private String comment;
    private Date startDate;
    private Date plannedEndDate;
    private Date endDate;

    public ProjectGetDto(long id, String description, List<ProjectEmployeeDto> employeeIds, long customerId,
                         String responsiblePerson, String comment, Date startDate,
                         Date plannedEndDate, Date endDate) {
        this.id = id;
        this.description = description;
        this.employees = employeeIds;
        this.customerId = customerId;
        this.responsiblePerson = responsiblePerson;
        this.comment = comment;
        this.startDate = startDate;
        this.plannedEndDate = plannedEndDate;
        this.endDate = endDate;
    }
}