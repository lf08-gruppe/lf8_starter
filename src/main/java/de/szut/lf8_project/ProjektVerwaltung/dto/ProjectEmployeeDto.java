package de.szut.lf8_project.ProjektVerwaltung.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProjectEmployeeDto {
    public long employeeId;
    public String role;
    public ProjectEmployeeDto(long employeeId, String role){
        this.employeeId = employeeId;
        this.role = role;
    }
}
